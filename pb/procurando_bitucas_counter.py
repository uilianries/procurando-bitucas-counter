#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging.handlers
import os
import configparser
import sys
import asyncio
import time
from datetime import datetime
import feedparser

import telegram
import emoji


BITUCAS_LOGGING_LEVEL = int(os.getenv("BITUCAS_LOGGING_LEVEL", 10))
logger = logging.getLogger(__name__)
logger.setLevel(BITUCAS_LOGGING_LEVEL)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
S_HANDLER = logging.StreamHandler()
S_HANDLER.setLevel(BITUCAS_LOGGING_LEVEL)
S_HANDLER.setFormatter(formatter)
logger.addHandler(S_HANDLER)


def is_under_maintenance():
    return os.getenv("BITUCAS_UNDER_MAINTENANCE", False)


def is_dry_run():
    return os.getenv("BITUCAS_DRY_RUN", False)


def config_file_path():
    return os.getenv("PB_CONFIG", "/etc/bitucas.conf")


def config_file():
    config_path = config_file_path()
    if not os.path.exists(config_path):
        raise ValueError("Could not obtain configuration file path.")
    config = configparser.ConfigParser()
    config.read(config_path)
    return config


def get_chat_id():
    token = os.getenv("BITUCAS_CHAT_ID", None)
    if not token:
        config = config_file()
        token = config["telegram"]["chat_id"]
    return token


def get_telegram_token():
    token = os.getenv("TELEGRAM_TOKEN", None)
    if not token:
        config = config_file()
        token = config["telegram"]["token"]
    return token


def get_rss_feed():
    rss = os.getenv("BITUCAS_RSS_FEED", None)
    if not rss:
        config = config_file()
        rss = config["rss"]["feed"]
    return rss


def get_last_episode_date(rss: str):
    rss_feed = feedparser.parse(rss)
    last_ep = rss_feed["entries"][0]
    return datetime.fromtimestamp(time.mktime(last_ep["published_parsed"]))


def days_since_last_ep(episode_date):
    now = datetime.now()
    diff = now - episode_date
    return diff.days


def get_emoji_based_on_interval(last_ep_days: int):
    if last_ep_days < 10:
        return ":winking_face:"
    elif last_ep_days < 20:
        return ":face_screaming_in_fear:"
    elif last_ep_days < 30:
        return ":crying_face:"
    elif last_ep_days < 40:
        return ":disappointed_face:"
    elif last_ep_days < 50:
        return ":tired_face:"
    elif last_ep_days < 60:
        return ":face_with_steam_from_nose:"
    elif last_ep_days < 70:
        return ":angry_face:"
    elif last_ep_days < 80:
        return ":skull:"
    else:
        return ":ghost:"


def format_telegram_message(last_ep_days: int):
    emoji_face = get_emoji_based_on_interval(last_ep_days)
    message_raw = f"Estamos sem episódios do Procurando Bitucas há {last_ep_days} dias. {emoji_face}"
    return emoji.emojize(message_raw)


async def send_message_to_telegram(message):
    max_retry = 5
    retry_count = 0
    while retry_count < max_retry:
        try:
            bot = telegram.Bot(token=get_telegram_token())
            if not is_dry_run():
                try:
                    async with bot:
                        await bot.send_message(chat_id=get_chat_id(), text=message)
                except Exception as error:
                    logger.error(f"Could not post message: {error}")
                    retry_count += 1
                    time.sleep(3)
                    continue
        except Exception as error:
            logger.error(f"Could not initialize Telegram: {error}")
            retry_count += 1
            time.sleep(3)
            continue
        break


def show_configuration():
    telegram_token = str(get_telegram_token())[:8]
    logger.info(f"CONFIG FILE: {config_file_path()}")
    logger.info(f"TELEGRAM TOKEN: {telegram_token}")
    logger.info(f"CHAT ID: {get_chat_id()}")
    logger.info(f"UNDER MAINTENANCE: {is_under_maintenance()}")
    logger.info(f"DRY RUN: {is_dry_run()}")


async def run():

    show_configuration()

    if is_under_maintenance():
        logging.warning("Procurando Bitucas is under maintenance. Exiting now.")
        sys.exit(503)

    rss = get_rss_feed()
    last_ep_date = get_last_episode_date(rss)
    days = days_since_last_ep(last_ep_date)
    message = format_telegram_message(days)
    logger.debug(message)
    await send_message_to_telegram(message)


def main():
    asyncio.run(run())


if __name__ == '__main__':
    main()
